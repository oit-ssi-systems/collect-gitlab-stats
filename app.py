#!/usr/bin/env python3

import sys
import gitlab
import argparse
import os
import hvac
from influxdb import InfluxDBClient


def parse_args():
    parser = argparse.ArgumentParser(
        description='Poll gitlab for statistics and log to influxdb')

    parser.add_argument('gitlab-server', help='Gitlab server to query')
    parser.add_argument('influxdb-server',
                        help='InfluxDB server to send data to')
    parser.add_argument('-k', '--kubernetes-auth', action='store_true',
                        help="Use k8s auth to generate a vault token")

    return parser.parse_args()


def main():

    args = vars(parse_args())
    print(args)

    if args['kubernetes_auth']:
        print("Kubernetes auth")
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        vault = hvac.Client(url=os.environ['VAULT_ADDR'])
        vault.auth_kubernetes("gitlab-stats", jwt,
                              mount_point='openshift-dev')
    else:
        print("VAULT_TOKEN auth")
        vault = hvac.Client(url=os.environ['VAULT_ADDR'],
                            token=os.environ['VAULT_TOKEN'])

    # Look up influxdb credentials
    influxdb_auth = vault.read(
        'secret/linux/influxdb/databases/gitlab/rw_users/gitlabrw')['data']

    influxdb_client = InfluxDBClient(
        args['influxdb-server'],
        username=influxdb_auth['username'],
        password=influxdb_auth['password'],
        ssl=True,
        verify_ssl=True
    )

    # Get gitlab audit account
    gitlab_auth = vault.read('secret/linux/gitlab/{}/audit'.format(
        args['gitlab-server']))['data']

    # Connect to gitlab
    gl = gitlab.Gitlab('https://{}'.format(args['gitlab-server']),
                       private_token=gitlab_auth['api_token'],
                       api_version=4
                       )
    gl.auth()

    # Pass these to all queries
    query_opts = {
        'as_list': False,
        'all': True,
        'obey_rate_limit': False
    }

    global_tags = {
        'gitlab_instance': args['gitlab-server'],
    }


    # Count Groups
    group_count = 0
    for item in gl.groups.list(**query_opts):
        group_count += 1

    group_data = [{
        'measurement': 'group_count',
        'tags': global_tags,
        'fields': {
            'value': group_count
        }
    }]

    # Send group data to influx
    influxdb_client.write_points(group_data, database='gitlab')
    print(group_data)

    # Count Users
    users = {}
    for item in gl.users.list(**query_opts):
        if item.state not in users:
            users[item.state] = 0

        users[item.state] += 1
    user_data = []
    for user_state, count in users.items():
        state_data = {
            'measurement': 'user_count',
            'tags': {
                **global_tags,
                'user_state': user_state
            },
            'fields': {
                'value': count
            }
        }
        user_data.append(state_data)
    influxdb_client.write_points(user_data, database='gitlab')
    print(user_data)

    # Count projects
    project_count = 0
    for item in gl.projects.list(**query_opts):
        project_count += 1

    project_data = [{
        'measurement': 'project_count',
        'tags': global_tags,
        'fields': {
            'value': project_count
        }
    }]

    # Send group data to influx
    influxdb_client.write_points(project_data, database='gitlab')
    print(project_data)
    return 0


if __name__ == "__main__":
    sys.exit(main())
